using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour, IDamageable
{
    private IAttack currentWeapon;
    [SerializeField] private float maxHealth;
    [SerializeField] private IDamageable healthCompontent;

    public void TakeDamage(float _damage)
    {
        throw new NotImplementedException();
    }

    void Start()
    {
        healthCompontent = GetComponent<IDamageable>();
        healthCompontent = new HealthComponent();
        currentWeapon = new Sword();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            TakeDamage(10);
        }
    }
}
