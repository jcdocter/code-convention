using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthComponent : MonoBehaviour, IDamageable
{
    public float health { get; set; }
    [SerializeField] private Slider healthSlider;

    public void TakeDamage(float _damage)
    {
        health -= _damage;
        healthSlider.value = health / maxHealth;

        if (health <= 0)
        {
            Die();
        }
    }
    private void Die()
    {

    }


}
