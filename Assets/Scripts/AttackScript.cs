using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackScript : MonoBehaviour, IAttack
{
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Attack(currentWeapon);
        }
    }

    private void Attack(Weapon _weaponType)
    {
        switch (_weaponType)
        {
            case Weapon.Bow:
                DoRangedAttack();
                break;

            case Weapon.Sword:
                DoMeleeAttack();
                break;
        }
    }

    private void DoMeleeAttack()
    {

    }

    private void DoRangedAttack()
    {

    }
}
